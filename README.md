# Android Architecture Example

1.  [CleanArchitechture](https://gitlab.com/tungtsdev96/android-architecture-example/tree/master/CleanArchitechture-master)
2.  [todo-mvp-clean](https://gitlab.com/tungtsdev96/android-architecture-example/tree/master/android-architecture-todo-mvp-clean)
3.  [todo-mvp-dagger](https://gitlab.com/tungtsdev96/android-architecture-example/tree/master/android-architecture-todo-mvp-dagger)
4.  [todo-mvp-rxjava](https://gitlab.com/tungtsdev96/android-architecture-example/tree/master/android-architecture-todo-mvp-rxjava)
5.  [mvp-interactor-architecture](https://gitlab.com/tungtsdev96/android-architecture-example/tree/master/android-mvp-interactor-architecture-master)
6.  [mvvm]

# Reference
1.  [https://github.com/NamTranDev/CleanArchitechture](https://github.com/NamTranDev/CleanArchitechture)
2.  [https://github.com/googlesamples/android-architecture](https://github.com/googlesamples/android-architecture)
3.  [https://github.com/MindorksOpenSource/android-mvp-interactor-architecture/blob/master/README.md](https://github.com/MindorksOpenSource/android-mvp-interactor-architecture/blob/master/README.md)

